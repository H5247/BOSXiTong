package com.crm.service;

import java.util.List;

import javax.jws.WebService;

import com.crm.bean.Customer;

@WebService
public interface CrmService {
	
	public List<Customer> findAll();
	
	public List<Customer> findCusByDecidedId(String decidedId);
	
	public List<Customer> findCusNoDecided();
	//给定区关联客户的方法  第一个参数定区Id  第二参数  客户Id的数组
	public void assigncustomerstodecidedzone(String decidedId,List<String> ids);
	
	//通过电话号码查询customer
	public Customer findCusByTel(String telephone);
	//通过地址查询定区的Id
	public String findDecidedIdByAddr(String address);
	
}
