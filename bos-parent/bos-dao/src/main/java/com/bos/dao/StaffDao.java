package com.bos.dao;

import com.bos.bean.PageBean;
import com.bos.bean.Staff;
import com.bos.dao.base.BaseDao;

public interface StaffDao extends BaseDao<Staff>{
	
}
