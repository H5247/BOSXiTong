package com.bos.dao;

import java.util.List;

import com.bos.bean.Region;
import com.bos.dao.base.BaseDao;

public interface RegionDao extends BaseDao<Region> {

	List<Region> findByQ(String q);

	
}
