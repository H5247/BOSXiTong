package com.bos.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.bos.bean.PageBean;
import com.bos.bean.Subarea;
import com.bos.dao.SubareaDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class SubareaDaoImpl extends BaseDaoImpl<Subarea> implements SubareaDao {

}
