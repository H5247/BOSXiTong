package com.bos.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.orm.hibernate5.HibernateCallback;
import org.springframework.stereotype.Repository;

import com.bos.bean.Staff;
import com.bos.dao.StaffDao;
import com.bos.dao.base.BaseDaoImpl;

import net.sf.json.JSONObject;

@Repository
public class StaffDaoImpl extends BaseDaoImpl<Staff> implements StaffDao {
	
	public void pageQuery()
	{
		
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	/*// 第一种 Session   
	Session session = getHibernateTemplate().getSessionFactory().getCurrentSession();
	String hql = "FROM Staff";
	//分页    
	List list = session.createQuery(hql).setFirstResult().setMaxResults(rows).list();
	//总数据量
	String hql1 = "SELECT count(*) FROM Staff";
	Long countLong = (Long) session.createQuery(hql1).uniqueResult();
	int count = countLong.intValue();
	System.out.println("total:" + count + "rows:" + list.toString());
	//将分页数据和总记录数  弄成JSON
	JSONObject json = new JSONObject();
	json.put("total", count);
	json.put("rows", list);*/
	/*
	 * 第二种
	 * getHibernateTemplate().execute(new HibernateCallback<T>() {
		public List doInHibernate(Session session) throws HibernateException {
			return null;
		}
	});*/
	
	/*DetachedCriteria criteria = DetachedCriteria.forClass(Staff.class);
	//第三种
	List rows = getHibernateTemplate().findByCriteria(criteria, 0, 20);
	
	// total
	criteria.setProjection(Projections.rowCount());  //  发送一个 SELECT count(*) FROM Staff
	//只有一条数据
	List<Long> countList = (List<Long>) getHibernateTemplate().findByCriteria(criteria);
	//取出来
	Long long1 = countList.get(0);
	int total = long1.intValue();*/
}
