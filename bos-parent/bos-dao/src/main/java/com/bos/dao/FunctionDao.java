package com.bos.dao;

import java.util.List;

import com.bos.bean.Function;
import com.bos.dao.base.BaseDao;

public interface FunctionDao extends BaseDao<Function> {

	List<Function> findByUserId(String id);

}
