package com.bos.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bos.bean.Function;
import com.bos.dao.FunctionDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class FunctionDaoImpl extends BaseDaoImpl<Function> implements FunctionDao {
	//通过用户Id查询权限 DISTINCT去重
	public List<Function> findByUserId(String id) {
		String hql = "SELECT DISTINCT f FROM Function f LEFT JOIN f.roles r LEFT JOIN r.users u where u.id = ?";
		return (List<Function>) getHibernateTemplate().find(hql, id);
	}
}
