package com.bos.dao;

import com.bos.bean.Subarea;
import com.bos.dao.base.BaseDao;

public interface SubareaDao extends BaseDao<Subarea> {

}
