package com.bos.dao;

import com.bos.bean.User;
import com.bos.dao.base.BaseDao;

public interface UserDao extends BaseDao<User>{
	
	User findUserByUserNameAndPwd(String userName,String password);

	User findUserByUserName(String username);
}
