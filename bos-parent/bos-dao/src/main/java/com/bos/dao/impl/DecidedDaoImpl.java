package com.bos.dao.impl;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.springframework.stereotype.Repository;

import com.bos.bean.DecidedZone;
import com.bos.bean.PageBean;
import com.bos.dao.DecidedDao;
import com.bos.dao.base.BaseDaoImpl;

@Repository
public class DecidedDaoImpl extends BaseDaoImpl<DecidedZone> implements DecidedDao {

}
