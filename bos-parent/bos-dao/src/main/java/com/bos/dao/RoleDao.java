package com.bos.dao;

import com.bos.bean.Role;
import com.bos.dao.base.BaseDao;

public interface RoleDao extends BaseDao<Role> {

}
