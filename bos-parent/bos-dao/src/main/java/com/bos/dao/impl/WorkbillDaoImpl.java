package com.bos.dao.impl;

import org.springframework.stereotype.Repository;

import com.bos.bean.Workbill;
import com.bos.dao.WorkbillDao;
import com.bos.dao.base.BaseDaoImpl;
@Repository
public class WorkbillDaoImpl extends BaseDaoImpl<Workbill> implements WorkbillDao {
}
