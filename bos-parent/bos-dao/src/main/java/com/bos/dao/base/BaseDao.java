package com.bos.dao.base;

import java.io.Serializable;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;

import com.bos.bean.PageBean;

public interface BaseDao<T> {
	//增删改查
	void  insert(T bean);
	
	void delete(T bean);
	
	void update(T bean);
	
	T findById(Serializable id);
	
	List<T> getAll();
	
	//添加一个分页查询的方法
	void pageQuery(PageBean pageBean);
	
	//扩展一个通用的根据条件更新的方法
	void updateByQueryName(String queryName,Object... parm);
	void saveOrUpdate(T bean);
	//扩展一个通过criteria 查询的方法
	List findByCriteria(DetachedCriteria criteria);
}
