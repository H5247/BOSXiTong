package com.bos.dao;

import com.bos.bean.Noticebill;
import com.bos.dao.base.BaseDao;

public interface NoticebillDao extends BaseDao<Noticebill> {
}
