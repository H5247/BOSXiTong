package com.bos.intercepter;

import org.apache.struts2.ServletActionContext;

import com.bos.bean.User;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.AbstractInterceptor;
import com.opensymphony.xwork2.interceptor.MethodFilterInterceptor;

//登录拦截器
public class LoginIntercepter extends MethodFilterInterceptor {

	@Override
	protected String doIntercept(ActionInvocation invocation) throws Exception {
		// 1.获取session中的userInfo
		User user = (User) ServletActionContext.getRequest().getSession().getAttribute("userInfo");
		if (user != null) {
			// 已登录
			// 放行
			return invocation.invoke();
		}
		return "login";
	}

}
