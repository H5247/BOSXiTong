package com.bos.action;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bos.bean.DecidedZone;
import com.bos.bean.Subarea;
import com.bos.crm.CrmService;
import com.bos.crm.Customer;
import com.bos.service.DecidedService;

@Controller
@Scope("prototype")
public class DecidedAction extends BaseAction<DecidedZone> {

	@Autowired
	private CrmService crmService;
	private List<String> customerIds;

	public void setCustomerIds(List<String> customerIds) {
		this.customerIds = customerIds;
	}

	// 获取页面提交的分区id数组
	private String[] subareaid;

	public void setSubareaid(String[] subareaid) {
		this.subareaid = subareaid;
	}

	@Autowired
	private DecidedService service;

	public String add() {
		service.save(model, subareaid);
		return LIST;
	}

	public String pageQuery() throws IOException {
		service.pageQuery(pageBean);
		objToJson(pageBean, new String[] { "decidedZones", "decidedZone", "region" });
		return NONE;
	}

	// 查询没有关联的客户
	public String crmnodecided() throws IOException {
		List<Customer> list = crmService.findCusNoDecided();
		listToJSON(list, new String[] {});
		return NONE;
	}

	// 查询已关联的客户
	public String crmhasdecided() throws IOException {
		List<Customer> list = crmService.findCusByDecidedId(model.getId());
		listToJSON(list, new String[] {});
		return NONE;
	}

	// 给定区关联客户
	public String assigncustomerstodecidedzone() {
		crmService.assigncustomerstodecidedzone(model.getId(), customerIds);
		return LIST;
	}

	// 分页查询已关联的客户
	// 查询已关联的客户
	public String associationCustomer() throws IOException {
		List<Customer> list = crmService.findCusByDecidedId(model.getId());
		pageBean.setRows(list);
		pageBean.setTotal(list.size());
		objToJson(pageBean);
		return NONE;
	}

	public String getSubAreaByDecidedId() throws IOException {
		DecidedZone decided = service.findById(model.getId());
		Set<Subarea> set = decided.getSubareas();
		setToJSON(set, new String[] { "decidedZone", "region" });
		return NONE;
	}

}
