package com.bos.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.apache.struts2.ServletActionContext;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bos.bean.PageBean;
import com.bos.bean.Staff;
import com.bos.service.StaffService;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

@Controller
@Scope("prototype")
public class StaffAction extends BaseAction<Staff> {
	@Autowired
	private StaffService staffService;
	
	private String ids;
	
	
	public String save(){
		//在方法内 可以加上权限验证
		Subject subject = SecurityUtils.getSubject();
		//检测是否有这个权限   如果没有 就会抛异常
		subject.checkPermission("staff-delete");
		staffService.save(model);
		return LIST;
	}
	
	public String update(){
		
		
		//先查询 
		Staff staff = staffService.findById(model.getId());
		//在设置
		staff.setName(model.getName());
		staff.setStandard(model.getStandard());
		staff.setStation(model.getStation());
		staff.setTelephone(model.getTelephone());
		staff.setHaspda(model.getHaspda());
		//在更新
		staffService.update(staff);
		return LIST;
	}
	
	//分页查询
	public String findByPage() throws IOException{
		staffService.pageQuery(pageBean);
		objToJson(pageBean,new String[]{"currentPage", "pageSize", "criteria","staff","decidedZone","region"});
		return NONE;
	}
	
	
	//批量删除   在方法之前执行  权限的判断
	@RequiresPermissions("staff-delete")
	public String delbatch(){
		staffService.delbatch(ids);
		return LIST;
	}
	
	public String querystaffnodel() throws IOException{
		List<Staff> staffs = staffService.findNoDel();
		//LIST转json
		listToJSON(staffs, new String[]{"decidedZones"});
		return NONE;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
}
