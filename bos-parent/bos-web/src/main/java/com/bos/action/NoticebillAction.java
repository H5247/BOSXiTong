package com.bos.action;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.bos.bean.Noticebill;
import com.bos.crm.CrmService;
import com.bos.crm.Customer;
import com.bos.service.NoticebillService;

@Controller
@Scope("prototype")
public class NoticebillAction extends BaseAction<Noticebill> {

	@Autowired
	private CrmService crmService;
	
	@Autowired
	private NoticebillService noticeService;
	
	
	public String findCusByTel() throws IOException{
		Customer cus = crmService.findCusByTel(model.getTelephone());
		//把customer给页面
		objToJson(cus, new String[]{});
		return NONE;
	}
	
	public String add(){
		noticeService.save(model);
		return NONE;
	}
	
}
