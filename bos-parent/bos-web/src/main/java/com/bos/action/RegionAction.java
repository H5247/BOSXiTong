package com.bos.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import com.bos.bean.Region;
import com.bos.service.RegionService;
import com.bos.utils.PinYin4jUtils;

@Controller
@Scope("prototype")
public class RegionAction extends BaseAction<Region> {

	private File regionFile;
	
	private String q;
	public void setQ(String q){
		this.q = q;
	}
	
	@Autowired
	private RegionService service;
	public void setRegionFile(File regionFile) {
		this.regionFile = regionFile;
	}

	public String importXls() throws FileNotFoundException, IOException{
		//判断
		//收到了excel表格
		HSSFWorkbook workbook = new HSSFWorkbook(new FileInputStream(regionFile));
		HSSFSheet sheet = workbook.getSheetAt(0);
		List<Region> regions = new ArrayList<Region>();
		for (Row row : sheet) {
			if(row.getRowNum() == 0){
				//跳过此次循环继续下次
				continue;
			}
			//创建一个region对象  保存到数据库
			Region region = new Region();
			String id = row.getCell(0).getStringCellValue();
			String province = row.getCell(1).getStringCellValue();
			String city = row.getCell(2).getStringCellValue();
			String district = row.getCell(3).getStringCellValue();
			String postcode = row.getCell(4).getStringCellValue();
			region.setPostcode(postcode);
			region.setId(id);
			region.setProvince(province);
			region.setCity(city);
			region.setDistrict(district);
			
			province = province.substring(0, province.length() -1);
			city = city.substring(0, city.length() -1);
			district = district.substring(0, district.length() -1);
			String str = province + city + district;
			//获取第一个汉字的首拼
			String[] headerArr = PinYin4jUtils.getHeadByString(str);
			// 转换成字符串
			StringBuilder sb = new StringBuilder();
			for (String string : headerArr) {
				sb.append(string);
			}
			String shortcode = sb.toString();
			String citycode = PinYin4jUtils.hanziToPinyin(city, "");
			//两个  使用拼音4j   简码  和城市编码     shortCode  cityCode
			region.setShortcode(shortcode);
			region.setCitycode(citycode);
			regions.add(region);
		}
		//Service  批量保存
		service.saveBatch(regions);
		return NONE;
	}
	
	public String pageQuery() throws IOException{
		service.pageQuery(pageBean);
		//排除分区
		objToJson(pageBean,new String[] { "currentPage", "pageSize", "criteria","subareas" });
		return NONE;
	}
	//导出Excel表格
	public String exportXls() throws IOException{
		//在内存中创建excel表格对象
		HSSFWorkbook workbook = new HSSFWorkbook();
		//创建一个sheet
		HSSFSheet createSheet = workbook.createSheet();
		//查询数据库查询到数据
		List<Region> regions = service.findAll();
		//第一行手动创建
		HSSFRow firstRow = createSheet.createRow(0);
		//手动创建列
		firstRow.createCell(0).setCellValue("区域编号");
		firstRow.createCell(1).setCellValue("省份");
		firstRow.createCell(2).setCellValue("城市");
		firstRow.createCell(3).setCellValue("区域");
		firstRow.createCell(4).setCellValue("邮编");
		
		for (Region region : regions) {
			//循环创建行
			HSSFRow currentRow = createSheet.createRow(createSheet.getLastRowNum() + 1);
			//循环的创建列
			currentRow.createCell(0).setCellValue(region.getId());
			currentRow.createCell(1).setCellValue(region.getProvince());
			currentRow.createCell(2).setCellValue(region.getCity());
			currentRow.createCell(3).setCellValue(region.getDistrict());
			currentRow.createCell(4).setCellValue(region.getPostcode());
		}
		
		HttpServletResponse response = ServletActionContext.getResponse();
		//输出文件   application/json;charset=utf-8  mime 类型
		//输出文件的时候  决定文件的 后缀名的是   header  所以  需要修改 响应的头
        response.setContentType("multipart/form-data");   
        //2.设置文件头：最后一个参数是设置下载文件名(假如我们叫a.pdf)   
        //将中文转换成  utf-8
        String fileName = "区域数据.xls";
        fileName = URLEncoder.encode(fileName, "utf-8");
        response.setHeader("Content-Disposition", "attachment;fileName="+fileName);   
		workbook.write(response.getOutputStream());
		return NONE;
	}
	
	public String listinfo() throws IOException{
		List<Region> regions = null;
		if(q != null && !q.isEmpty()){
			//模糊查询
			regions = service.findByQ(q);
		}else{
			regions = service.findAll();
		}
		//获取Q 根据q 来模糊查询
		//查询所有的区域
		listToJSON(regions, new String[]{"subareas"});
		return NONE;
	}
}
