package com.bos.action;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.SessionAware;
import org.hibernate.criterion.DetachedCriteria;

import com.bos.bean.PageBean;
import com.bos.bean.Staff;
import com.bos.bean.User;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

public class BaseAction<T> extends ActionSupport implements ModelDriven<T>, SessionAware {

	protected final static String HOME = "home";
	protected final static String LOGIN = "login";
	protected final static String LIST = "list";

	protected Map<String, Object> session;

	protected PageBean pageBean = new PageBean();
	DetachedCriteria criteria;

	// Struts框架设置值 是通过set方法
	public void setPage(int page) {
		pageBean.setCurrentPage(page);
	}

	public void setRows(int rows) {
		pageBean.setPageSize(rows);
	}

	protected T model;

	public BaseAction() {
		ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
		Type[] actualTypeArguments = genericSuperclass.getActualTypeArguments();
		Class clazz = (Class) actualTypeArguments[0];
		// 设置criteria的类型
		criteria = DetachedCriteria.forClass(clazz);
		pageBean.setCriteria(criteria);
		try {
			model = (T) clazz.newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	// 将java对象 转换据成JSON数
	protected void objToJson(Object obj, String[] configArr) throws IOException {
		// 排除那些属性不需要转成JSON
		JsonConfig config = new JsonConfig();
		// 设置排除的属性为|
		config.setExcludes(configArr);
		JSONObject jsonObj = JSONObject.fromObject(obj, config);
		// 向客户端发送jSON
		// 获取resp
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json;charset=utf-8");
		// 获取一个writer
		PrintWriter writer = response.getWriter();
		writer.print(jsonObj.toString());
		writer.close();
	}

	protected void objToJson(Object obj) throws IOException {

		// 排除那些属性不需要转成JSON
		JsonConfig config = new JsonConfig();
		// 设置排除的属性为|
		config.setExcludes(new String[] { "currentPage", "pageSize", "criteria" });
		JSONObject jsonObj = JSONObject.fromObject(obj, config);
		// 向客户端发送jSON
		// 获取resp
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json;charset=utf-8");
		// 获取一个writer
		PrintWriter writer = response.getWriter();
		writer.print(jsonObj.toString());
		writer.close();
	}

	protected void listToJSON(List list, String[] configArr) throws IOException {
		// 排除那些属性不需要转成JSON
		JsonConfig config = new JsonConfig();
		// 设置排除的属性为|
		config.setExcludes(configArr);
		JSONArray jsonObj = JSONArray.fromObject(list, config);
		// 向客户端发送jSON
		// 获取resp
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json;charset=utf-8");
		// 获取一个writer
		PrintWriter writer = response.getWriter();
		writer.print(jsonObj.toString());
		writer.close();
	}
	
	
	protected void setToJSON(Set set, String[] configArr) throws IOException {
		// 排除那些属性不需要转成JSON
		JsonConfig config = new JsonConfig();
		// 设置排除的属性为|
		config.setExcludes(configArr);
		JSONArray jsonObj = JSONArray.fromObject(set, config);
		// 向客户端发送jSON
		// 获取resp
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setContentType("application/json;charset=utf-8");
		// 获取一个writer
		PrintWriter writer = response.getWriter();
		writer.print(jsonObj.toString());
		writer.close();
	}

	public T getModel() {
		return model;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;
	}

}
