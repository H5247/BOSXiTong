<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>
</head>
<body>
	
		
	<!-- 重点掌握 -->
	<table id = "dataGridTb"></table>
	<script type="text/javascript">
		//文档加载好之后的事件   
		$(function(){
			var currentIndex;
			$("#dataGridTb").datagrid(
				{
					url:'${pageContext.request.contextPath}/json/datagrid.json',
					columns:[[
					          //将这一列变成复选框
					          {field:'id',title:"编号",width:100,editor:{type:'validatebox',options:{required:true}}},
					          {field:'name',title:"姓名",width:200,editor:{type:'validatebox',options:{required:true}}},
					          {field:'age',title:"年龄",width:200,editor:{type:'validatebox',options:{required:true}}}
					          ]],
					//工具栏
					toolbar:[{
						text:"添加",
						iconCls:'icon-add',
						handler:function(){
							if(currentIndex != undefined){
								//结束编辑
								$("#dataGridTb").datagrid("endEdit",currentIndex);
							}							
							//添加一行
							if(currentIndex == undefined){
								currentIndex = 0;
								$("#dataGridTb").datagrid("insertRow",{
									index:currentIndex,
									row:{}
								});
								//编辑这一行
								$("#dataGridTb").datagrid("beginEdit",currentIndex);
							}
						}
					},{text:"保存",iconCls:'icon-save',handler:function(){
						//结束编辑
						$("#dataGridTb").datagrid("endEdit",0);
					}}
					],
					//分页条
					pagination:true,
					onAfterEdit:function(rowIndex, rowData, changes){
						//发送请求
						currentIndex = undefined;
						alert("保存成功");
					}
				}
			);
		})	
	
	
	</script>

</body>
</html>