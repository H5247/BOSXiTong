<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/default/easyui.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/js/easyui/themes/icon.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.8.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/easyui/jquery.easyui.min.js"></script>

</head>
<body>
	<!-- 第一种方式 -->
	<select name="" class = "easyui-combobox" style="width: 200px" >
		<option>第一项</option>
		<option>第2项</option>
		<option>第3项</option>
		<option>第4项</option>
	</select>
	
	<!-- 第二种动态生成 
	 提交到服务器  提交到服务器的值     显示给用户看的值
	-->
	<input id = "cc" class = "easyui-combobox" name = "aaa"  
		data-options = "
		valueField:'id',
		textField:'name',
		url:'${pageContext.request.contextPath}/json/combobox.json',
		mode:'remote'
		"
	  >
</body>
</html>