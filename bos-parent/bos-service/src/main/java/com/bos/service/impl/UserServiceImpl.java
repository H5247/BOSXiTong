package com.bos.service.impl;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.PageBean;
import com.bos.bean.Role;
import com.bos.bean.User;
import com.bos.dao.UserDao;
import com.bos.service.UserService;
import com.bos.utils.MD5Utils;

@Service
@Transactional  //事物注解
public class UserServiceImpl implements UserService{

	//注入dao
	@Autowired
	private UserDao userDao;
	
	public User login(String userName, String password) {
		password = MD5Utils.md5(password);
		return userDao.findUserByUserNameAndPwd(userName, password);
	}

	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		userDao.update(user);
	}

	@Override
	public void save(User model, String[] roleId) {
		//将密码MD5 加密
		String password = MD5Utils.md5(model.getPassword());
		model.setPassword(password);
		//保存用户
		userDao.insert(model);
		//给用户关联角色
		for (String id : roleId) {
			//通过id创建角色对象   托管对象
			Role role = new Role(id);
			//给用户设置角色
			model.getRoles().add(role);
		}
	}

	public void pageQuery(PageBean pageBean) {
		userDao.pageQuery(pageBean);
	}
}
