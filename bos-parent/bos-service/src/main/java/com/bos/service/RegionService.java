package com.bos.service;

import java.util.List;

import com.bos.bean.PageBean;
import com.bos.bean.Region;

public interface RegionService {
	void saveBatch(List<Region> regions);

	void pageQuery(PageBean pageBean);

	List<Region> findAll();

	List<Region> findByQ(String q);
	
}
