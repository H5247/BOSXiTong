package com.bos.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.DecidedZone;
import com.bos.bean.PageBean;
import com.bos.bean.Subarea;
import com.bos.dao.DecidedDao;
import com.bos.dao.SubareaDao;
import com.bos.service.DecidedService;

@Service
@Transactional
public class DecidedServiceImpl implements DecidedService {
	
	@Autowired
	private DecidedDao decidedDao;
	
	@Autowired
	private SubareaDao subareaDao;
	
	//保存定区
	public void save(DecidedZone model, String[] subareaid) {
		decidedDao.insert(model);
		//循环查询出所有id对应的分区  
		//将分区绑定到定区
		for (String id : subareaid) {
			Subarea subarea = subareaDao.findById(id);
			//给持久化的对象设置值
			subarea.setDecidedZone(model);
		}
	}
	public void pageQuery(PageBean pageBean) {
		decidedDao.pageQuery(pageBean);
	}
	@Override
	public DecidedZone findById(String id) {
		return decidedDao.findById(id);
	}
}
