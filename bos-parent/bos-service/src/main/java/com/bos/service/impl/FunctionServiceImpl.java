package com.bos.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.Function;
import com.bos.bean.PageBean;
import com.bos.dao.FunctionDao;
import com.bos.service.FunctionService;

@Service
@Transactional
public class FunctionServiceImpl implements FunctionService {
	
	@Autowired
	private FunctionDao functionDao;

	public void pageQuery(PageBean pageBean) {
		functionDao.pageQuery(pageBean);
	}

	public List<Function> findAll() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Function.class);
		//设置父节点为null的 查询条件
		criteria.add(Restrictions.isNull("parentFunction"));
		return functionDao.findByCriteria(criteria);
	}
	
}
