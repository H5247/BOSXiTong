package com.bos.service.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.PageBean;
import com.bos.bean.Subarea;
import com.bos.dao.SubareaDao;
import com.bos.service.SubareaService;

@Service
@Transactional
public class SubareaServiceImpl implements SubareaService {

	@Autowired
	private SubareaDao dao;

	@Override
	public void save(Subarea m) {
		dao.insert(m);
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		dao.pageQuery(pageBean);
	}

	@Override
	public List<Subarea> findNoRelevance() {
		DetachedCriteria criteria = DetachedCriteria.forClass(Subarea.class);
		//定区为空
		criteria.add(Restrictions.isNull("decidedZone"));
		return dao.findByCriteria(criteria);
	}
}
