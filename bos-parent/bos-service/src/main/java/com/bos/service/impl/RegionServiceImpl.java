package com.bos.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bos.bean.PageBean;
import com.bos.bean.Region;
import com.bos.dao.RegionDao;
import com.bos.service.RegionService;


@Service
@Transactional
public class RegionServiceImpl implements RegionService {

	@Autowired
	private RegionDao dao;
	
	public void saveBatch(List<Region> regions) {
		for (Region region : regions) {
			//保存或者更新
			dao.saveOrUpdate(region);
		}
	}

	@Override
	public void pageQuery(PageBean pageBean) {
		// TODO Auto-generated method stub
		dao.pageQuery(pageBean);
	}

	@Override
	public List<Region> findAll() {
		return dao.getAll();
	}

	@Override
	public List<Region> findByQ(String q) {
		/* private String province;	//省份
     private String city;		
     private String district;	//区
     private String shortcode;	//简码         	BJBJDC
     private String citycode;	//城市编码 	beijing
		 * */
		return dao.findByQ(q);
	}
}
