package com.bos.service;

import java.util.List;

import com.bos.bean.Function;
import com.bos.bean.PageBean;

public interface FunctionService {

	void pageQuery(PageBean pageBean);

	List<Function> findAll();

}
